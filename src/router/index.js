import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Timetable from '../views/Timetable.vue';
import Test from '../views/Test.vue';
import Stat from '../views/Stat.vue';
import Course from '../views/Course.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/timetable',
    name: 'timetable',
    component: Timetable,
  },
  {
    path: '/timetable/:page',
    name: 'timetable',
    component: Timetable,
  },
  {
    path: '/course',
    name: 'course',
    component: Course,
  },
  {
    path: '/course/:id',
    name: 'course',
    component: Course,
  },
  {
    path: '/test',
    name: 'test',
    component: Test,
  },
  {
    path: '/stat',
    name: 'stat',
    component: Stat,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
