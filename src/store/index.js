import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    current: '1',
    courses: [
      {
        id: '1',
        name: 'Математика',
      },
      {
        id: '2',
        name: 'Русский язык',
      },
    ],
    timetable: [{"id":"1","courseId":"1","title":"Сложение","startedAt":"1571745600000"},{"id":"2","courseId":"1","title":"Вычитание","startedAt":"1571824800000"},{"id":"3","courseId":"2","title":"Буквы","startedAt":"1571842800000"},{"id":"4","courseId":"2","title":"Слова","startedAt":"1571929200000"},{"id":"5","courseId":"1","title":"Умножение","startedAt":"1571914800000"},{"id":"6","courseId":"2","title":"Предложения","startedAt":"1571990400000"},{"id":"7","courseId":"1","title":"Деление","startedAt":"1572004800000"},{"id":"8","courseId":"2","title":"Словосочетания","startedAt":"1572091200000"},{"id":"9","courseId":"2","title":"Предлоги","startedAt":"1572107400000"},{"id":"10","courseId":"1","title":"Квадратные корни","startedAt":"1572184800000"},{"id":"11","courseId":"1","title":"Дроби","startedAt":"1572087600000"},{"id":"12","courseId":"1","title":"Логарифмы","startedAt":"1572253200000"}], // eslint-disable-line
  },
  getters: {
    links: state => id => [
      ...Array(Math.ceil(state.timetable.filter(v => v.courseId === id).length / 5)).keys(),
    ],
    course: state => id => state.timetable.find(v => v.id === id),
    current: state => state.current,
    courses: state => state.courses,
    timetable: state => (id, page) => state.timetable.filter(v => (v.courseId - 0) === (id - 0)).slice(page * 5, page * 5 + 5), // eslint-disable-line
  },
  mutations: {
    current: (state, v) => {
      state.current = v;
    },
  },
  actions: {
  },
  modules: {
  },
});
